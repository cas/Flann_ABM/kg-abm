from ontology import *


a = Law('law1')
a.vote('1990-01-03')
a.enforce('1991-05-25')
a.createCredit(name= 'Extension Tram 15 a Saint-Julien', date= '2021-05-03', startP= 'start points coordinates', endP= 'end point coordinates')
a.createCredit('Extension Tram 18 a Saint-Genis', '2015-11-27', 'start points coordinates', 'end point coordinates')

importCommunes('communes.csv')
showCommunes()

for c in listCommunes[3:7]:
    c.changePolicy()
    
showObjects()

exportKG()