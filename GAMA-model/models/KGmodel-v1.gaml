model KGmodel

/*
 * KG model v0 builds homes randomly inside each commune.
 * KG model v0 treats people as spatial, standalone agents.
 * KG model v1 integrates homes and people inside a grid. 
 * Possible improvements: have all initial parameters in a par dict (map) to easily import and export it.
 */
global {
	date starting_date <- date("1980-01-01");
	float step <- 1 #day;
	// Load shapefiles
	string PDC_path <- "../includes/wholeLayers/PDC_layer.shp";
	string communesBordersPath <- '../includes/wholeLayers/communesGG_removeSpecialCharsGE_subset3.shp';
	string quartiersPath <- '../includes/wholeLayers/VDG_QUARTIER_VILLE.shp';
	string lakePath <- '../includes/wholeLayers/GEO_LAC.shp';
	string tpgTramsPath <- '../pythonKG/inshp/initialNetwork.shp';
	string AddressesPath <- "../includes/wholeLayers/AGGLO_CARREAU_200_subset2.shp"; //Population data from OFS and INSEE 2019.
	string RoadsPath <- "../includes/wholeLayers/cleanRoads_GG_Merge3.shp";

	// Load KG
	string kgPath <- '../pythonKG/exports/';
	string communeskgPath <- kgPath + 'communes.csv';
	string creditskgPath <- kgPath + 'credits.csv';
	string lawskgPath <- kgPath + 'laws.csv';
	string lineskgPath <- '../pythonKG/outshp/all_lines_lineObj.shp';

	// Define output files
	string outputPath <- "../pythonKG/model_outputs/";
	string readme <- outputPath + 'readme.txt';
	string outputCredits <- outputPath + "gen_credits.csv";
	string outputLines <- outputPath + "gen_lines.csv";
	string outputCellsSHP <- outputPath + "cells.shp";
	string outputTramLinesSHP <- outputPath + "tramLines.shp";
	string outputCreditsSHP <- outputPath + "credits.shp";

	// Set world boundaries. Warning, this HAS to be done in global BEFORE init, otherwise everything breaks.
	// Which means that the envelope-defining file also needs to be defined in global before init.
	geometry shape <- envelope(file(communesBordersPath));

	// Initial Parameter values (user-defined)
	date startPopulationGrowth <- date('2019-01-01');
	date startPrediction <- date('2020-01-01');
	date endSim <- date("2060-01-01");
	bool split_GE_into_districts <- false;
	int demographicGrowth <- 18;
	float globalPar_initialExpansionScore <- 100.0;
	float globalPar_expansionScoreThreshold <- 10.0;
	float expansion_score_multiplier_from_new_people <- 0.0;
	float expansion_score_change_from_move_in <- 1.0;
	float expansion_score_neighbor_change_from_move_in <- 1.0; //divided by the amount of neighbors
	float expansion_score_change_from_new_residential <- -3.0;
	float maxTramLineLength <- 8000.0;
	float clusterDistance <- 500.0;
	float proportionToCluster <- 0.1;
	float credit_creation_delay_months <- 6.0;
	float line_creation_delay_months <- 24.0;
	float searchRadius_peopleAroundPath <- 500.0;
	float searchRadius_peopleAroundCells <- 500.0;
	float globalPar_proximityRadius <- 400.0;
	float searchRadius_construction <- 400.0;
	float searchRadius_peopleAroundRoads <- 400.0;
	int globalPar_maxBuiltPerCell <- 100;
	int globalPar_initialDecisionInterval <- 3 * 366;
	float globalPar_decision_interval_multiplier_on_rejection <- 0.9;
	float globalPar_score_threshold_multiplier_on_rejection <- 0.2;
	float globalPar_demographic_growth_influence_multiplier <- 2.0;
	float exp_nPaP <- 1.0;
	float exp_dist <- 2.0; // Formula is nPeople around path ^ exp_nPaP / length of path ^ exp_dist;

	// Weighted graph
	float weight_population <- 1.0; // Perimeter is divided by 1+x, where x is equal to road's people reached (normalized to [0;1]) * weight_population. 1+x is in [1; 1+weight_population].	
	map<Roads, float> weightedRoads;
	graph weightedRoadsGraph;

	// Variables
	bool predictionMode <- false;
	float score_multiplier <- 1000000.0;
	int nExtension <- 0;
	int newPeople;
	int totalEmptyHomes <- 0;
	list<Cells> cellsWithVacancies;
	Cells housingCell;
	bool expansionLegalised <- false;
	int clID <- 0;
	list initialLineChoices <- ["12", "13", "14", "15", "17", "18"];
	list lineChoices <- initialLineChoices;

	// UI elements
	point selected_location;
	list<Attractors> historyAttractors;
	list<point> attractorLocations;
	string toSave <- "";
	list toLoad;
	string savedAttractorsPath <- "../includes/savedAttractors.txt";

	init {
		do initReadme;
		create Cells from: file(AddressesPath) with: [nPeople::int(read("POP_TOT_00"))];
		ask Cells inside geometry(lake) {
			do die;
		}

		create Communes from: file(communesBordersPath) with: [name:: string(read("COMMUNE")), country::string(read("PAYS"))];
		if split_GE_into_districts {
			ask Communes first_with (each.name = "Geneve") {
				do die;
			}

			create Communes from: file(quartiersPath) with: [name:: string(read("NOM_QUARTIER")), country::'Suisse'];
		}

		do initBaseMap;
		//do cleanRoads; //activate when a new roads shapefile is generated, then deactivate when clean shp is generated.
		create Roads from: file(RoadsPath);
		create PDC_areas from: file(PDC_path) with: [type:: string(read("Type")), lon::float(read("CENTROID_X")), lat::float(read("CENTROID_Y"))];
		create Government;
		do initKG;
		do initOutputs;
		do findCommuneNeighbors;
		do initRoadWeightedGraph;
		do initCounters;
		ask Communes {
			if not (empty(tramLines overlapping self)) {
				do declareTargetCellsForConstruction;
			} else if not (empty(Credits overlapping self)) {
				do declareTargetCellsForConstruction;
			}

		}

		ask Cells {
			do updateStatus;
		}

	}

	action initReadme {
		save 'Date of beginning of population growth: ' + startPopulationGrowth to: readme format: txt header: false rewrite: true;
		save 'Date of beginning of prediction mode: ' + startPrediction to: readme format: txt header: false rewrite: false;
		save 'Date of simulation end: ' + endSim to: readme format: txt header: false rewrite: false;
		save 'Split Geneva into its districts: ' + split_GE_into_districts to: readme format: txt header: false rewrite: false;
		save 'Demographic growth per day: ' + demographicGrowth to: readme format: txt header: false rewrite: false;
		save 'Initial commune expansion score: ' + globalPar_initialExpansionScore to: readme format: txt header: false rewrite: false;
		save 'Expansion score threshold (to trigger main reflex): ' + globalPar_expansionScoreThreshold to: readme format: txt header: false rewrite: false;
		save 'Expansion score multiplier from new people not yet residing: ' + expansion_score_multiplier_from_new_people to: readme format: txt header: false rewrite: false;
		save 'Expansion score change from person moving in: ' + expansion_score_change_from_move_in to: readme format: txt header: false rewrite: false;
		save 'Expansion score change from person moving in a neighboring commune: ' + expansion_score_neighbor_change_from_move_in to: readme format: txt header: false rewrite: false;
		save 'Expansion score change from building a residential: ' + expansion_score_change_from_new_residential to: readme format: txt header: false rewrite: false;
		save 'Maximum tram line length: ' + maxTramLineLength to: readme format: txt header: false rewrite: false;
		save 'Minimum distance between clusters: ' + clusterDistance to: readme format: txt header: false rewrite: false;
		save 'Proportion of top populated cells to cluster: ' + proportionToCluster to: readme format: txt header: false rewrite: false;
		save 'Credit creation process length [months]: ' + credit_creation_delay_months to: readme format: txt header: false rewrite: false;
		save 'Tram line creation process length [months]: ' + line_creation_delay_months to: readme format: txt header: false rewrite: false;
		save 'Search radius for people around paths: ' + searchRadius_peopleAroundPath to: readme format: txt header: false rewrite: false;
		save 'Search radius for people around cells: ' + searchRadius_peopleAroundCells to: readme format: txt header: false rewrite: false;
		save 'Maximum distance for cells to be considered near a tram line or credit: ' + globalPar_proximityRadius to: readme format: txt header: false rewrite: false;
		save 'Selecting cells for new construction - search radius around tram lines and credits: ' + searchRadius_construction to: readme format: txt header: false rewrite: false;
		save 'Search radius for people around roads: ' + searchRadius_peopleAroundRoads to: readme format: txt header: false rewrite: false;
		save 'Weight of population in weighted road graph: ' + weight_population to: readme format: txt header: false rewrite: false;
		save 'Maximum amount of new residentials per cell: ' + globalPar_maxBuiltPerCell to: readme format: txt header: false rewrite: false;
		save 'Government intitial decision interval for approving a new credit: ' + globalPar_initialDecisionInterval to: readme format: txt header: false rewrite: false;
		save 'Government decision interval multiplier on rejection: ' + globalPar_decision_interval_multiplier_on_rejection to: readme format: txt header: false rewrite: false;
		save 'Government score threshold multiplier on rejection: ' + globalPar_score_threshold_multiplier_on_rejection to: readme format: txt header: false rewrite: false;
		save 'Government score threshold multiplier from proportion of new people with no residence / total resident population: ' + globalPar_demographic_growth_influence_multiplier
		to: readme format: txt header: false rewrite: false;
		save 'Path score exponent of number of people around path: ' + exp_nPaP to: readme format: txt header: false rewrite: false;
		save 'Path score exponent of path length: ' + exp_dist to: readme format: txt header: false rewrite: false;
	}

	action initBaseMap {
		create lake from: file(lakePath) with: [name:: string(read("NOM"))];
		create tramLines from: file(tpgTramsPath) with: [lineID::string(read("LIGNE"))] {
			do setColor;
			built <- true;
		}

	}

	action cleanRoads {
		write "Cleaning network...";
		list<geometry> clean_roads <- clean_network(file(RoadsPath).contents, 1, false, false);
		save clean_roads to: "../includes/wholeLayers/cleanRoads_GG_Merge3.shp" format: shp;
		write "Done, file saved for future use.";
	}

	action initKG {
		do initCommunes;
		do initLaws;
		do initCredits;
		do initNewLines;
	}

	action initOutputs {
		save "creditName;requesterCommuneName;dateCreated" to: outputCredits format: txt header: false rewrite: true;
		save "lineName;parentCreditName;dateCreated" to: outputLines format: txt header: false rewrite: true;
	}

	action initCommunes {
		loop c over: rows_list(matrix(csv_file(communeskgPath, ';', true))) {
			ask Communes where (each.name = c[0]) {
				allow <- c[1] = 'True';
			}

		}

	}

	action initLaws {
		loop l over: rows_list(matrix(csv_file(lawskgPath, ';', true))) {
			create Laws {
				location <- {0, 0};
				name <- l[0];
				if l[1] != 'None' {
					dateVoted <- l[1];
				}

				if l[2] != 'None' {
					dateEnforced <- l[2];
				}

			}

		}

	}

	action initCredits {
		loop c over: rows_list(matrix(csv_file(creditskgPath, ';', true))) {
			create Credits {
				location <- {0, 0};
				name <- c[0];
				dateCreated <- c[1];
				parentLaw <- Laws first_with (each.name = c[2]);
			}

		}

	}

	action initNewLines {
		create tramLines from: file(lineskgPath) with: [name:: string(read("linename")), lineID::string(read("number")), dateBuilt::date(string(read("date")))] {
			color <- #white;
			parentCredit <- Credits first_with (each.name = self.name);
			ask parentCredit {
				childLine <- myself;
			}

		}

	}

	action findCommuneNeighbors {
		ask Communes {
			neighbors <- topology(self) neighbors_of self;
		}

	}

	action initRoadWeightedGraph {
		float m <- weight_population / Roads max_of (each.nPeopleAround);
		//weightedRoads <- Roads as_map (each::(each.shape.perimeter / (1)));
		weightedRoads <- Roads as_map (each::(each.shape.perimeter / (1 + (m * each.nPeopleAround))));
		graph g <- as_edge_graph(Roads);
		weightedRoadsGraph <- as_edge_graph(Roads) with_weights weightedRoads;
	}

	action initCounters {
		cellsWithVacancies <- Cells where (each.nEmptyHomes > 0);
	}

	action init_p2 { //WIP
		do setUpAttractors;
	}

	action setUpAttractors { //WIP
	}

	action clustering (list<Cells> cellsToCluster, bool disp <- false) {
		cellsToCluster <- reverse(cellsToCluster sort_by (each.nPeopleAround));
		cellsToCluster <- copy_between(cellsToCluster, 0, int(length(cellsToCluster) * proportionToCluster));
		list<list<Cells>> clusters <- cellsToCluster simple_clustering_by_distance (clusterDistance);
		if disp {
			ask Cells {
				do resetAppearance;
			}

			int nMax <- length(clusters);
			int i <- 0;
			loop cl over: clusters {
				ask cl {
					color <- rgb(255 * (1 - i / nMax), 0, 0, 255);
					info <- string(i);
				}

				i <- i + 1;
			}

		}

		return clusters;
	}

	action createTramProjects (list<list<Cells>> clusters, string requesterID) {
		loop cl over: clusters {
			clID <- clID + 1;
			loop c over: cl {
				tramLines tl <- tramLines where (each.built) closest_to self;
				point p2 <- (c closest_points_with tl)[1];
				path p <- weightedRoadsGraph path_between (c, p2);
				if p != nil and p.shape.perimeter <= maxTramLineLength {
					create Paths {
						lineID <- tl.lineID;
						reqID <- requesterID;
						shape <- p.shape;
						nPaP <- cl at_distance searchRadius_peopleAroundPath sum_of (each.nPeople);
						score <- (nPaP ^ exp_nPaP) / (shape.perimeter ^ exp_dist) * score_multiplier;
						p_clID <- clID;
					}

				} else {
				}

			}

			ask Paths where (each.p_clID = clID and each.score < (Paths where (each.p_clID = clID)) max_of (each.score)) {
				if p_com = nil {
					do die;
				}

			}

		}

	}

	action createLinesBetweenAttractors {
		list<Attractors> la1 <- [];
		loop a1 over: Attractors {
			add a1 to: la1;
			loop a2 over: Attractors - la1 {
				path p <- weightedRoadsGraph path_between (a1, a2);
				create tramLines {
				//if p = nil {
					shape <- line(a1, a2);
					//} else {
					//shape <- p.shape;
					//}
					if length(lineChoices) <= 0 {
						lineChoices <- initialLineChoices;
					}

					lineID <- one_of(lineChoices);
					remove lineID from: lineChoices;
					built <- true;
					do setColor;
				}

			}

		}

		endSim <- date("2100-01-01");
	}

	/*****************************************/
	/************* MAIN REFLEXES *************/
	/************* OF THE MODEL **************/
	/*****************************************/
	reflex main {
		if current_date >= startPopulationGrowth {
			do increasePopulation;
		}

		if newPeople > 0 {
			do houseNewPeople;
			do increaseAllScores;
		}

		if current_date = startPrediction {
			predictionMode <- true;
		}

		if current_date = endSim {
			do exportOutputs;
			do pause;
		}

	}

	action exportOutputs {
		save tramLines to: outputTramLinesSHP format: shp attributes: ["name"::name, "lineID"::lineID, "dateBuilt"::dateBuilt, "parentCredit"::parentCredit, "built"::built];
		save Credits to: outputCreditsSHP format: shp attributes: ["name"::name, "childLine"::childLine, "dateCreated"::dateCreated, "created"::created];
		save Cells to: outputCellsSHP format: shp attributes: ["nPeople"::nPeople, "nPeopleAround"::nPeopleAround, "nNewHomes"::nNewHomes, "nEmptyHomes"::nEmptyHomes];
	}

	action increasePopulation {
	// in 2023, 6577 new inhabitants in Geneva -> 18 per day.
		newPeople <- newPeople + demographicGrowth;
	}

	action houseNewPeople {
		loop while: (newPeople > 0 and totalEmptyHomes > 0) {
			housingCell <- one_of(cellsWithVacancies where (each.status = "near tram line"));
			if housingCell = nil {
				housingCell <- one_of(cellsWithVacancies);
			}

			ask housingCell {
				nEmptyHomes <- nEmptyHomes - 1;
				if nEmptyHomes = 0 {
					remove self from: cellsWithVacancies;
				}

				nPeople <- nPeople + 1;
				com.expansionScore <- com.expansionScore + expansion_score_change_from_move_in;
				ask com.neighbors where (each.allow) {
					expansionScore <- expansionScore + expansion_score_neighbor_change_from_move_in / length(myself.com.neighbors);
				}

			}

			totalEmptyHomes <- totalEmptyHomes - 1;
			newPeople <- newPeople - 1;
		}

	}

	action increaseAllScores {
		ask Communes where (each.allow) {
			expansionScore <- expansionScore + newPeople * expansion_score_multiplier_from_new_people;
		}

	}

}

/********************************************************/
/***************** MAIN 3 AGENT SPECIES *****************/
/************* CELLS, COMMUNES, GOVERNMENT **************/
/********************************************************/
species Cells schedules: [] {
	int nPeople;
	int nPeopleAround;
	float searchRadius <- searchRadius_peopleAroundCells;
	float proximityRadius <- globalPar_proximityRadius;
	int nEmptyHomes <- 0;
	int nNewHomes <- 0;
	rgb color <- #transparent;
	string info;
	Communes com;
	string status;
	bool full;

	init {
		nPeopleAround <- Cells at_distance searchRadius sum_of (each.nPeople) + nPeople;
	}

	action updateStatus {
		if not (empty(tramLines at_distance proximityRadius)) {
			status <- "near tram line";
		} else if not (empty(Credits at_distance proximityRadius)) {
			status <- "near credit";
		} else {
			status <- "isolated";
		}

	}

	action resetAppearance {
		color <- #transparent;
		info <- nil;
	}

	aspect base {
		draw shape border: rgb(128, 128, 128, 30) color: color;
		draw info color: #black;
	}

}

species Communes {
	bool allow;
	string country;
	rgb countryColor;
	float expansionScore;
	float initialExpansionScore;
	tramLines closestLine;
	point p2;
	bool awaitingCredit <- false;
	list<Communes> neighbors;
	list<Cells> cellsWithin;
	list<Cells> targetCells;
	Cells cityCentre;
	int maxBuiltPerCell <- globalPar_maxBuiltPerCell;
	Paths submittedPath;
	bool projectCooldown <- false;
	float expansionScoreThreshold <- globalPar_expansionScoreThreshold;

	init {
		if country = 'Suisse' {
			countryColor <- rgb(255, 190, 190, 102);
		} else if country = 'France' {
			countryColor <- rgb(254, 186, 15, 51);
		}

		allow <- true;
		initialExpansionScore <- globalPar_initialExpansionScore;
		expansionScore <- initialExpansionScore;
		cellsWithin <- Cells inside self;
		ask cellsWithin {
			com <- myself;
		}

	}

	action createTramProject {
		projectCooldown <- true;
		ask world {
			do createTramProjects(clustering(myself.cellsWithin, true), myself.name);
		}

		submittedPath <- Paths where (each.reqID = name) with_max_of (each.score);
		if submittedPath != nil {
			ask submittedPath {
				p_com <- myself;
			}

		}

		ask Paths where (each.reqID = name) - submittedPath {
			if p_com = nil {
				do die;
			}

		}

		if submittedPath != nil {
			ask Government {
				add myself to: queue;
			}

			awaitingCredit <- true;
		}

	}

	action generateNewCredit {
		create tramLines {
			shape <- myself.submittedPath.shape;
			color <- #red;
			lineID <- myself.submittedPath.lineID;
			name <- 'Line extension ' + nExtension + ' (tram ' + lineID + ')';
			nExtension <- nExtension + 1;
			create Credits {
				dateCreated <- current_date + credit_creation_delay_months #months;
				myself.parentCredit <- self;
				childLine <- myself;
			}

			dateBuilt <- parentCredit.dateCreated + line_creation_delay_months #months;
			save parentCredit.name + ";" + myself.name + ";" + parentCredit.dateCreated to: outputCredits format: txt header: false rewrite: false;
			save name + ";" + parentCredit.name + ";" + dateBuilt to: outputLines format: txt header: false rewrite: false;
		}

		ask Cells {
			do updateStatus;
		}

		ask submittedPath {
			do die;
		}

		do declareTargetCellsForConstruction;
	}

	action build (Cells targetCell, int nBuilt) {
		ask targetCell {
			nNewHomes <- nNewHomes + nBuilt;
			nEmptyHomes <- nEmptyHomes + nBuilt;
			if not (self in cellsWithVacancies) {
				add self to: cellsWithVacancies;
			}

			if nNewHomes >= myself.maxBuiltPerCell {
				full <- true;
				remove self from: myself.targetCells;
			}

		}

		totalEmptyHomes <- totalEmptyHomes + nBuilt;
		expansionScore <- expansionScore + expansion_score_change_from_new_residential * nBuilt;
	}

	action declareTargetCellsForConstruction {
		ask Credits overlapping self {
			myself.targetCells <- myself.targetCells + myself.cellsWithin where (not (each.full)) at_distance searchRadius_construction;
		}

		ask tramLines overlapping self {
			myself.targetCells <- myself.targetCells + myself.cellsWithin where (not (each.full)) at_distance searchRadius_construction;
		}

	}

	reflex main when: (predictionMode and expansionLegalised and allow and (expansionScore > expansionScoreThreshold) and not (awaitingCredit) and not (empty(neighbors where (not
	(empty(tramLines overlapping each)))))) {
		if empty(tramLines overlapping self) {
			if not (projectCooldown) {
				cityCentre <- cellsWithin with_max_of (each.nPeopleAround);
				do createTramProject;
			}

		} else if not (empty(targetCells)) {
			do build(one_of(targetCells), 1);
		}

	}

	aspect base {
		draw shape color: countryColor border: #black;
	}

}

species Government {
	date nextDecision <- startPrediction;
	int initialDecisionInterval <- globalPar_initialDecisionInterval;
	int decisionInterval <- initialDecisionInterval;
	float decision_interval_multiplier_on_rejection <- globalPar_decision_interval_multiplier_on_rejection;
	float score_threshold_multiplier_on_rejection <- globalPar_score_threshold_multiplier_on_rejection;
	list<Communes> queue;
	Communes target;
	float scoreThreshold;
	list historyGrantsScores;
	float demographic_growth_influence_multiplier <- globalPar_demographic_growth_influence_multiplier;

	action approveCredit {
		ask Paths {
			color <- #transparent;
		}

		write "Initial score threshold is " + scoreThreshold with_precision 2;
		scoreThreshold <- scoreThreshold * (1 - demographic_growth_influence_multiplier * newPeople / (Cells sum_of (each.nPeople)));
		write "Initial score threshold reduced by " + demographic_growth_influence_multiplier * 100 * newPeople / (Cells sum_of (each.nPeople)) with_precision
		0 + "% down to " + scoreThreshold with_precision 2;
		if not (empty(queue)) {
			target <- queue with_max_of (each.submittedPath.score);
			target.submittedPath.color <- #blue;
			if target.submittedPath.score >= scoreThreshold {
				add target.submittedPath.score to: historyGrantsScores;
				scoreThreshold <- float(max(historyGrantsScores));
				decisionInterval <- initialDecisionInterval;
				write target.name + "authorized for creating a new credit, setting threshold score to " + scoreThreshold with_precision 2;
				queue <- nil;
				ask target {
					do generateNewCredit;
				}

			} else {
				scoreThreshold <- scoreThreshold * score_threshold_multiplier_on_rejection;
				decisionInterval <- int(max(31, decisionInterval * decision_interval_multiplier_on_rejection));
				write "No new credits created, setting threshold score to " + scoreThreshold with_precision 2 + ", new decision in " + decisionInterval with_precision 2 + " days.";
			}

		}

		ask Communes {
			projectCooldown <- false;
			awaitingCredit <- false;
		}

	}

	reflex main when: current_date = nextDecision { // Make a decision every X days.
		do approveCredit;
		nextDecision <- current_date + decisionInterval #days;
	}

}

species lake schedules: [] {

	aspect base {
		draw shape color: rgb(208, 234, 245) border: #black;
	}

}

species tramLines {
	string lineID;
	rgb color;
	date dateBuilt;
	Credits parentCredit;
	bool built <- false;

	init {
	}

	action setColor {
		if lineID = "12" {
			color <- rgb(245, 163, 0);
		} else if lineID = "13" {
			color <- rgb(112, 188, 84);
		} else if lineID = "14" {
			color <- rgb(90, 30, 130);
		} else if lineID = "15" {
			color <- rgb(132, 71, 28);
		} else if lineID = "17" {
			color <- rgb(0, 172, 231);
		} else if lineID = "18" {
			color <- rgb(184, 47, 137);
		} }

	reflex build when: current_date = dateBuilt {
		built <- true;
		do setColor;
		write 'Line "' + name + '" has been created';
	}

	aspect base {
		draw shape color: color width: 100;
	} }

	//*******************************************//
//*************** PDC AREAS *****************//
//*******************************************//
species PDC_areas {
	rgb color;
	string type;
	float lon;
	float lat;
	bool potentialNewHomes <- true;
	bool new <- false;

	init {
		if "habitation" in type {
		// camo green if housing densification, light green if housing extension.
			if "Densification" in type {
				color <- rgb(132, 153, 137);
			} else if "Extension" in type {
				color <- rgb(215, 250, 224);
			}

		} else if "village" in type {
		// teal if villages
			color <- rgb(147, 241, 229);
		} else if "activités" in type {
		// pink if activites.
			color <- rgb(250, 215, 244);
			potentialNewHomes <- false;
		} else if "mixte" in type {
		// orange if renouvellement mixte.
			color <- rgb(255, 131, 89);
		} else {
		// light yellow otherwise.
			color <- rgb(255, 254, 222);
			potentialNewHomes <- false;
		} }

	aspect base {
		draw shape color: color;
	} }

species Credits {
	date dateCreated;
	Laws parentLaw;
	tramLines childLine;
	bool created <- false;

	reflex createCredit when: not (created) and current_date = dateCreated {
		created <- true;
		write 'Credit "' + name + '" has been created';
	}

}

species Laws {
	date dateVoted;
	bool voted <- false;
	date dateEnforced;
	bool enforced <- false;

	reflex vote when: not (voted) and current_date >= dateVoted {
		voted <- true;
		write 'Law "' + name + '" has been voted';
	}

	reflex enforce when: not (enforced) and current_date >= dateEnforced {
		enforced <- true;
		expansionLegalised <- true;
		write 'Law "' + name + '" has been enforced';
	}

}

species Roads schedules: [] {
	float searchRadius <- searchRadius_peopleAroundRoads;
	int nPeopleAround;
	rgb color <- #transparent;

	init {
		nPeopleAround <- Cells at_distance searchRadius sum_of (each.nPeople);
	}

	aspect base {
		draw shape color: color;
	}

}

species Paths schedules: [] {
	int nPaP;
	float score;
	int p_clID;
	string reqID;
	string lineID;
	Communes p_com <- nil;
	rgb color <- #transparent;

	aspect base {
		draw shape color: color;
	}

}

species Attractors schedules: [] {
	rgb color <- #black;

	init {
		add self to: historyAttractors;
	}

	aspect base {
		draw hexagon(300) color: color;
	}

}

experiment KG_model type: gui {

	action get_location {
		selected_location <- #user_location;
	}

	action confirm_Attractors { //WIP
		ask world {
			do init_p2;
		}

	}

	// Save attractors to txt file.
	action save_Attractors {
		bool first <- true;
		loop a over: Attractors {
			if first {
				toSave <- toSave + string(a.location);
				first <- false;
			} else {
				toSave <- toSave + ";" + string(a.location);
			}

		}

		save toSave to: savedAttractorsPath format: text;
		write "Attractors successfully saved!";
	}

	// Load attractors from txt file.
	action load_Attractors {
		if file_exists(savedAttractorsPath) {
			ask Attractors {
				do die;
			}

			toLoad <- string(text_file(savedAttractorsPath)) split_with ";";
			loop x over: toLoad {
				create Attractors {
					location <- x;
				}

			}

			write "Attractors successfully loaded.";
		} else {
			write "File does not exist !";
		}

	}

	action create_Attractor {
		create Attractors {
			location <- selected_location;
		}

	}

	action cancel_creation {
		ask historyAttractors[length(historyAttractors) - 1] {
			remove self from: historyAttractors;
			do die;
		}

	}

	action delete_Attractors {
		ask Attractors {
			do die;
		}

	}

	action delete_Network {
		ask tramLines {
			do die;
		}

		ask Credits {
			do die;
		}

	}

	action create_Lines_between_Attractors {
		ask world {
			do createLinesBetweenAttractors;
		}

	}

	action save_outputs {
		ask world {
			do exportOutputs;
		}

	}

	user_command "Save outputs" action: save_outputs category: "Outputs";
	user_command "Create Attractor" action: create_Attractor category: "Attractors";
	user_command "Save attractors" action: save_Attractors category: "Attractors";
	user_command "Load attractors" action: load_Attractors category: "Attractors";
	user_command "Remove the last created attractor" action: cancel_creation category: "Attractors";
	user_command "Delete all attractors" action: delete_Attractors category: "Attractors";
	user_command "Confirm all attractors" action: confirm_Attractors category: "Attractors";
	user_command "Delete network" action: delete_Network category: "AI-generated network";
	user_command "Create tram lines between attractors" action: create_Lines_between_Attractors category: "AI-generated network";
	output {
		display Environment type: java2D {
		//species Laws;
		//species Credits;
			species Communes aspect: base;
			species PDC_areas aspect: base;
			species lake aspect: base;
			species tramLines aspect: base;
			species Cells aspect: base;
			//species Government;
			species Paths aspect: base;
			species Attractors aspect: base;
			//species Roads;

			// Do action "get_location" whenever the user left clicks on the map.
			event 'mouse_down' action: get_location;

			// Draw the red cross used to highlight the last location clicked. It updates on each mouse click.
			graphics "Selected Location Highlight" {
				if selected_location != nil {
					draw cross(60, 15) color: #red border: #black at: selected_location;
				}

			}

		}

		//display "Charts" type: java2D {
		//chart "Amount of new people in the system" type: series {
		//data "newPeople" value: newPeople color: #green;
		//}

		//}
		monitor "Date" value: current_date refresh: every(1 #cycle);
	}

}


