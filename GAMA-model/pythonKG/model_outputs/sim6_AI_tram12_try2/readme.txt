Date of beginning of population growth: 2019-01-01 00:00:00
Date of beginning of prediction mode: 2020-01-01 00:00:00
Date of simulation end: 2060-01-01 00:00:00
Demographic growth per day: 18
Initial commune expansion score: 100.0
Expansion score threshold (to trigger main reflex): 10.0
Expansion score multiplier from new people not yet residing: 0.0
Expansion score change from person moving in: 1.0
Expansion score change from person moving in a neighboring commune: 1.0
Expansion score change from building a residential: -3.0
Maximum tram line length: 8000.0
Minimum distance between clusters: 500.0
Proportion of top populated cells to cluster: 0.1
Credit creation process length [months]: 6.0
Tram line creation process length [months]: 24.0
Search radius for people around paths: 500.0
Search radius for people around cells: 500.0
Maximum distance for cells to be considered near a tram line or credit: 400.0
Selecting cells for new construction - search radius around tram lines and credits: 400.0
Search radius for people around roads: 400.0
Weight of population in weighted road graph: 1.0
Maximum amount of new residentials per cell: 100
Government intitial decision interval for approving a new credit: 1098
Government decision interval multiplier on rejection: 0.9
Government score threshold multiplier on rejection: 0.8
Government score threshold multiplier from proportion of new people with no residence / total resident population: 2.0
Path score exponent of number of people around path: 1.0
Path score exponent of path length: 2.0
