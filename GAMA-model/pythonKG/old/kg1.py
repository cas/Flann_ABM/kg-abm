from ontology import *


a = Law('law1')
a.vote('1990-03-03')
a.enforce('1991-05-25')
a.createCredit(name= 'Extension Tram 15 a Ferney', date= '2024-01-11', startP= 'start points coordinates', endP= 'end point coordinates')
a.createCredit('Extension Tram 18 au CERN', '2010-11-01', 'start points coordinates', 'end point coordinates')

importCommunes('communes.csv')
showCommunes()

for c in listCommunes[3:7]:
    c.changePolicy()
    
showObjects()

exportKG()