model KGmodel

global {
	date starting_date <- date("1990-01-01");
	float step <- 1 #day;
	// Load shapefiles
	string PDC_path <- "../includes/wholeLayers/PDC_layer.shp";
	string communesBordersPath <- '../includes/wholeLayers/communesGG_removeSpecialCharsGE_subset3.shp';
	string lakePath <- '../includes/wholeLayers/GEO_LAC.shp';
	//string tpgTramsPath <- '../includes/wholeLayers/tpgTRAMS.shp';
	string tpgTramsPath <- '../pythonKG/inshp/initialNetwork.shp';
	string tpgStopsPath <- '../includes/wholeLayers/tpgStops.shp';
	string kgPath <- '../pythonKG/exports/';
	string communeskgPath <- kgPath + 'communes.csv';
	string creditskgPath <- kgPath + 'credits.csv';
	string lawskgPath <- kgPath + 'laws.csv';
	string lineskgPath <- '../pythonKG/outshp/all_lines_lineObj.shp';

	// Define output files
	string outputPath <- "../pythonKG/model_outputs/";
	string outputCredits <- outputPath + "gen_credits.csv";
	string outputLines <- outputPath + "gen_lines.csv";

	// Set world boundaries. Warning, this HAS to be done in global BEFORE init, otherwise everything breaks.
	// Which means that the envelope-defining file also needs to be defined in global before init.
	geometry shape <- envelope(file(communesBordersPath));

	// Parameter values
	float neighborsSearchRadius <- 5000.0;
	float recruitEstateInfluenceValue <- 3.0;
	float movingInInfluenceValue <- 1.0;
	int nExtension <- 0;

	init {
		do initBaseMap;
		create PDC_areas from: file(PDC_path) with: [type:: string(read("Type")), lon::float(read("CENTROID_X")), lat::float(read("CENTROID_Y"))];
		create EstateDev;
		create Government;
		do initKG;
		do initOutputs;
		do findCommuneNeighbors(neighborsSearchRadius);
	}

	action initBaseMap {
		create Communes from: file(communesBordersPath) with: [name:: string(read("COMMUNE")), country::string(read("PAYS"))];
		create lake from: file(lakePath) with: [name:: string(read("NOM"))];
		create tramLines from: file(tpgTramsPath) with: [lineID::string(read("LIGNE"))] {
			do setColor;
			built <- true;
		}

	}

	action initKG {
		do initCommunes;
		do initLaws;
		do initCredits;
		do initNewLines;
	}

	action initOutputs {
		save "creditName;requesterCommuneName;dateCreated" to: outputCredits format: txt header: false rewrite: true;
		save "lineName;parentCreditName;dateCreated" to: outputLines format: txt header: false rewrite: true;
	}

	action initCommunes {
		loop c over: rows_list(matrix(csv_file(communeskgPath, ';', true))) {
			ask Communes where (each.name = c[0]) {
				allow <- c[1] = 'True';
			}

		}

	}

	action initLaws {
		loop l over: rows_list(matrix(csv_file(lawskgPath, ';', true))) {
			create Laws {
				location <- {0, 0};
				name <- l[0];
				if l[1] != 'None' {
					dateVoted <- l[1];
				}

				if l[2] != 'None' {
					dateEnforced <- l[2];
				}

			}

		}

	}

	action initCredits {
		loop c over: rows_list(matrix(csv_file(creditskgPath, ';', true))) {
			create Credits {
				location <- {0, 0};
				name <- c[0];
				dateCreated <- c[1];
				parentLaw <- Laws first_with (each.name = c[2]);
			}

		}

	}

	action initNewLines {
		create tramLines from: file(lineskgPath) with: [name:: string(read("linename")), lineID::string(read("number")), dateBuilt::date(string(read("date")))] {
			color <- #white;
			parentCredit <- Credits first_with (each.name = self.name);
			ask parentCredit {
				childLine <- myself;
			}

		}

	}

	action findCommuneNeighbors (float radius <- 5000.0) {
		ask Communes {
			neighbors <- topology(self) neighbors_of self;
		}

	}

	reflex main {
		do increasePopulation(18);
	}

	reflex mainPeople when: not (empty(Buildings where (each.open and not (each.inhabited)))) {
		ask People {
			do moveIn;
		}

	}

	action increasePopulation (int demographicGrowth) {
	// in 2023, 6577 new inhabitants in Geneva -> 18 per day.
		create People number: demographicGrowth {
		}

	}

}

species Communes {
	bool allow;
	string country;
	rgb countryColor;
	geometry freeSpace;
	float expansionScore <- 0.0;
	point p1;
	tramLines closestLine;
	point p2;
	bool awaitingCredit <- false;
	list<Communes> neighbors;
	list<Communes> targetNeighbors;

	init {
		if country = 'Suisse' {
			countryColor <- rgb(245, 234, 218);
		} else if country = 'France' {
			countryColor <- rgb(247, 215, 215);
			allow <- true;
		}

		if name = 'Ville-la-Grand' {
			expansionScore <- 100.0;
		}

		do updateFreeSpace;
	}

	action updateFreeSpace (float freeSpaceBufferRadius <- 10.0) {
		freeSpace <- geometry(self) - (geometry(tramLines overlapping self) + freeSpaceBufferRadius) - (geometry(Buildings overlapping self) + freeSpaceBufferRadius);
	}

	action exploreExpansionFeasability {
		if empty(tramLines overlapping self) {
			do requestCredit;
		} else {
			do recruitEstate;
		}

	}

	action recruitEstate {
		ask EstateDev {
			do createBuildings targetCommune: myself;
		}

		expansionScore <- expansionScore - 10.0;
		do influenceNeighbors;
	}

	action requestCredit {
		if not (empty((tramLines where (each.built)) at_distance 5000)) and not (awaitingCredit) {
			ask Government {
				add myself to: queue;
			}

			awaitingCredit <- true;
		}

	}

	action generateNewCredit {
		p1 <- centroid(self);
		closestLine <- tramLines closest_to self;
		p2 <- (p1 closest_points_with closestLine)[1];
		create tramLines {
			shape <- line(myself.p1, myself.p2);
			color <- #red;
			lineID <- myself.closestLine.lineID;
			name <- 'Line extension ' + nExtension + ' (tram ' + lineID + ')';
			nExtension <- nExtension + 1;
			create Credits {
				dateCreated <- current_date + 6 #months;
				myself.parentCredit <- self;
				childLine <- myself;
			}

			dateBuilt <- parentCredit.dateCreated + 2 #years;
			save parentCredit.name + ";" + myself.name + ";" + parentCredit.dateCreated to: outputCredits format: txt header: false rewrite: false;
			save name + ";" + parentCredit.name + ";" + dateBuilt to: outputLines format: txt header: false rewrite: false;
		}

		awaitingCredit <- false;
		do updateFreeSpace;
	}

	action influenceNeighbors {
		targetNeighbors <- neighbors where (each.allow);
		if not (empty(targetNeighbors)) {
			ask targetNeighbors {
				expansionScore <- expansionScore + recruitEstateInfluenceValue;
			}

		}

	}

	action testCreateLines {
		if empty(tramLines overlapping self) {
			do requestCredit;
		}

	}

	reflex main when: (allow and (expansionScore > 10)) {
		do exploreExpansionFeasability;
	}

	aspect base {
		draw shape color: countryColor border: #black;
	}

}

species lake schedules: [] {

	aspect base {
		draw shape color: rgb(208, 234, 245) border: #black;
	}

}

species tramLines {
	string lineID;
	rgb color;
	date dateBuilt;
	Credits parentCredit;
	bool built <- false;

	init {
	}

	action setColor {
		if lineID = "12" {
			color <- rgb(245, 163, 0);
		} else if lineID = "13" {
			color <- rgb(112, 188, 84);
		} else if lineID = "14" {
			color <- rgb(90, 30, 130);
		} else if lineID = "15" {
			color <- rgb(132, 71, 28);
		} else if lineID = "17" {
			color <- rgb(0, 172, 231);
		} else if lineID = "18" {
			color <- rgb(184, 47, 137);
		} }

	reflex build when: current_date = dateBuilt {
		built <- true;
		do setColor;
		write 'Line "' + name + '" has been created';
	}

	aspect base {
		draw shape color: color;
	} }

	//*******************************************//
//*************** PDC AREAS *****************//
//*******************************************//
species PDC_areas {
	rgb color;
	string type;
	float lon;
	float lat;
	bool potentialNewHomes <- true;
	bool new <- false;

	init {
		if "habitation" in type {
		// camo green if housing densification, light green if housing extension.
			if "Densification" in type {
				color <- rgb(132, 153, 137);
			} else if "Extension" in type {
				color <- rgb(215, 250, 224);
			}

		} else if "village" in type {
		// teal if villages
			color <- rgb(147, 241, 229);
		} else if "activités" in type {
		// pink if activites.
			color <- rgb(250, 215, 244);
			potentialNewHomes <- false;
		} else if "mixte" in type {
		// orange if renouvellement mixte.
			color <- rgb(255, 131, 89);
		} else {
		// light yellow otherwise.
			color <- rgb(255, 254, 222);
			potentialNewHomes <- false;
		} }

	aspect base {
		draw shape color: color;
	} }

species Credits {
	date dateCreated;
	Laws parentLaw;
	tramLines childLine;
	bool created <- false;

	reflex createCredit when: current_date = dateCreated {
		created <- true;
		write 'Credit "' + name + '" has been created';
	}

}

species Laws {
	date dateVoted;
	bool voted <- false;
	date dateEnforced;
	bool enforced <- false;

	reflex vote when: current_date = dateVoted {
		voted <- true;
		write 'Law "' + name + '" has been voted';
	}

	reflex enforce when: current_date = dateEnforced {
		enforced <- true;
		write 'Law "' + name + '" has been enforced';
	}

}

species EstateDev {
	bool idle <- true;

	action createBuildings (Communes targetCommune, int nBuildings <- 1, geometry buildingShape <- square(10), float freeSpaceBufferRadius <- 10.0, int daysToBuild <- 180) {
		create Buildings number: nBuildings {
			if targetCommune.freeSpace != nil {
				inhabited <- false;
				open <- false;
				city <- targetCommune;
				dateBuilt <- current_date + daysToBuild #days;
				shape <- buildingShape;
				location <- any_location_in(targetCommune.freeSpace);
				ask targetCommune {
					do updateFreeSpace(freeSpaceBufferRadius);
				}

			} else {
				do die;
			}

		}

	}

	aspect base {
		draw square(50) color: #blue;
	}

}

species Buildings {
	bool inhabited;
	bool open;
	date dateBuilt;
	Communes city; // the city where the building is located.
	reflex open when: current_date = dateBuilt {
		open <- true;
	}

	aspect base {
		draw shape color: #grey;
	}

}

species Government {
	list<Communes> queue;
	Communes target;

	action approveCredit {
		if not (empty(queue)) {
			target <- queue with_max_of (each.expansionScore);
			ask target {
				do generateNewCredit;
			}

			remove target from: queue;
		}

	}

	reflex main when: copy_between(string(current_date), 5, 10) = '01-01' { // check if date is January 1.
		do approveCredit;
	}

}

species People {
	Buildings home;
	list<Communes> targetNeighbors;

	action influenceCommunes {
		targetNeighbors <- (home.city.neighbors) where (each.allow);
		if not (empty(targetNeighbors)) {
			ask targetNeighbors {
				expansionScore <- expansionScore + movingInInfluenceValue;
			}

		}

	}

	action moveIn {
		if not (empty(Buildings where (each.open and not (each.inhabited)))) {
			home <- one_of(Buildings where (each.open and not (each.inhabited)));
			ask home {
				inhabited <- true;
			}

			do influenceCommunes;
			do die;
		}

	}

}

experiment KG_model type: gui {
	output {
		display Environment type: java2D {
			species Laws;
			species Credits;
			species Communes aspect: base;
			species PDC_areas aspect: base;
			species lake aspect: base;
			species tramLines aspect: base;
			species Buildings aspect: base;
			species EstateDev aspect: base;
			species Government;
			//species tpgStops aspect: base;
		}

		monitor "Date" value: current_date refresh: every(1 #cycle);
	}

}


