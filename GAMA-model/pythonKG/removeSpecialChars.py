import pandas as pd
import geopandas as gpd

inDF = gpd.read_file('../includes/wholeLayers/communesGE.shp')
outfilePath = '../includes/wholeLayers/communesGE_removeSpecialChars.shp'

print(inDF)

communesDF = pd.read_csv('communes.csv', sep=';')
communesDF.columns=['cname', 'allow']
inDF = inDF.sort_values(by= ['NO_COMMUNE']).reset_index(drop=True)

print(inDF)
print(communesDF)


inDF.COMMUNE = communesDF.cname

print(inDF)

inDF.to_file(outfilePath)
