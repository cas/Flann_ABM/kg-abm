# Welcome to the full document and code base for the article by Chambers et al., 2024 and the KG-Agent-based model

The image "gama-model-screenshot.png" is a screenshot of the model in the GAMA platform right after simulation initialisation.

## Folder **GAMA-model**

This folder contains the model source code (folder **models**, sections 4.3 and 4.5.3) and input data (folder **includes**, section 4.4), as well as the python package for building the ontology and knowledge graphs (folder **pythonKG**, section 4.5.2). The folder contains all outputs produced by the simulations (sections 4.5.4 and 5), which are then typically moved into the **pythonKG/model_outputs** folder.