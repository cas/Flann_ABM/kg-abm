import pandas as pd
import geopandas as gpd
import os, shutil
import networkx as nx
import gravis as gv
from shapely.geometry import LineString

listLaws = []
listCredits = []
listCommunes = []
listLines = []
linesGDF = gpd.GeoDataFrame()
G = nx.DiGraph()

class Law:
    def __init__(self, name):
        self.name = name
        self.listCredits = []
        self.dateVoted = None
        self.dateEnforced = None
        self.dateModified = None
        listLaws.append(self)
        G.add_node(f"Law {self.name}")
    
    def vote(self, date):
        self.dateVoted = date
        G.add_edge(f"Law {self.name}", date, label= 'voted')

    def enforce(self, date):
        if self.dateVoted:
            self.dateEnforced = date
            G.add_edge(f"Law {self.name}", date, label= 'enforced')
        else:
            print('Error when enforcing the law: please vote the law first.')

    def createCredit(self, name, date):
        
        if self.dateEnforced:
            listCreditNames = [c.name for c in listCredits]
            if name in listCreditNames:
                print('Warning when creating credits: your chosen credit name is identical to the name of an other already existing credit. A number has been appended to the end of your name.')
                name += '2'
                i=3
                while name in listCreditNames:
                    name = name[:-1] + str(i)
                    i+=1
            c = Credit(name, date, self.name)
            self.listCredits.append(c)
            G.add_edge(f"Law {self.name}", f"Credit {name}", label= f'created {date}')
            return c
        else:
            print('Error when creating credits: please enforce the law first.')

    def modify(self, date):
        self.dateModified = date
        G.add_edge(f"Credit {self.name}", date, label= 'modified')

    def showAttributes(self):
        print(
            f"Law name: {self.name}\nDate voted: {self.dateVoted}\nDate enforced: {self.dateEnforced}\nDate modified: {self.dateModified}\n"
        )

    def returnAttributes(self):
        return f"{self.name};{self.dateVoted};{self.dateEnforced};{self.dateModified}\n"

class Credit:
    def __init__(self, name, date, parentLawName):
        self.name = name
        self.date = date
        self.parentLawName = parentLawName
        listCredits.append(self)
    
    def showAttributes(self):
        print(
            f"Credit name: {self.name}\nDate created: {self.date}\nParent Law: {self.parentLawName}\n"
        )

    def createLine(self, number, date, spatialData):
        li = Line(self.name, number, date, spatialData)
        listLines.append(li)
        G.add_edge(f"Credit {self.name}", f"Line {self.name}", label= f'created {date}')
        return li


    def returnAttributes(self):
        return f"{self.name};{self.date};{self.parentLawName}\n"

class Line:
    def __init__(self, name, number, date, spatialData):
        self.name = name
        self.number = number
        self.date = date
        self.spatialData = spatialData
        self.gdf = None
        self.linegdf = None
        

    def parseData(self, reverseCoordinates=False, reproj=None, input_crs = "EPSG:2056"):
        if type(self.spatialData) == str:
            if self.spatialData[-4:] == '.shp':
                self.linegdf = parseSHP(self, input_crs) #input is a shp
            elif self.spatialData[-4:] == '.txt' or self.spatialData[-4:] == '.csv':
                print('Parsing csv or txt files is not implemented yet.') #input is a csv or txt.
        elif type(self.spatialData) == pd.DataFrame:
            print('Parsing dataframes is not implemented yet.') #input is a dataframe, for instance read from a csv
        elif type(self.spatialData) == list:
            if type(self.spatialData[0]) == str:
                print('Parsing a list of stop names is not implemented yet.') #input is a list of stop names
            else: #input is a list of coordinates
                self.gdf, self.linegdf = parseCoordinates(self, reverseCoordinates, reproj)

    def showAttributes(self):
        print(
            f"Line number: {self.number}\nDate created: {self.date}\nParent credit name: {self.name}"
        )

    def exportSHP(self):
        if type(self.gdf) == gpd.GeoDataFrame:
            self.gdf.to_file(f"outshp/line{self.number}_{self.date}_stops.shp")
        if type(self.linegdf) == gpd.GeoDataFrame:
            self.linegdf.to_file(f"outshp/line{self.number}_{self.date}_lineObj.shp")

def parseSHP(li: Line, input_crs = "EPSG:2056"):
    global linesGDF

    linegdf = gpd.read_file(li.spatialData)

    linegdf['linename'] = li.name
    linegdf['number'] = li.number
    linegdf['date'] = li.date

    linesGDF = gpd.GeoDataFrame( pd.concat( [linesGDF, linegdf], ignore_index=True)) 

    return linegdf


def parseCoordinates(li: Line, reverseCoordinates=False, reproj=None):
    global linesGDF

    data = li.spatialData
    if type(data[0]) == tuple:
        data = [list(x) for x in data]

    if reverseCoordinates:
        data = [x[::-1] for x in data]

    data = [ [li.name] + ['stop' + str(i+1)] + x + [i+1] for i, x in enumerate(data) ]

    df = pd.DataFrame(data, columns = ['linename', 'name', 'lon', 'lat', 'stopN'])
    gdf = gpd.GeoDataFrame(
        df, geometry=gpd.points_from_xy(df.lon, df.lat), crs="EPSG:4326"
    )
    gdf = gdf.drop(['lon', 'lat'], axis=1)

    lineObj = LineString( [[p.x, p.y] for p in gdf.geometry.values] )
    linedf = pd.DataFrame()
    linedf['linename'] = [li.name,]
    linedf['number'] = [li.number,]
    linedf['date'] = [li.date,]
    linegdf = gpd.GeoDataFrame(linedf, geometry=[lineObj], crs="EPSG:4326")

    if reproj:
        linegdf= linegdf.to_crs(reproj) #EPSG:2056 is the CH1903+ / LV95 Swiss projection system.
        gdf= gdf.to_crs(reproj) #EPSG:2056 is the CH1903+ / LV95 Swiss projection system.

    linesGDF = gpd.GeoDataFrame( pd.concat( [linesGDF, linedf], ignore_index=True)) # linedf or linegdf ??


    return gdf, linegdf


class Commune:
    def __init__(self, name, allow):
        self.name = name
        self.allow = allow
        listCommunes.append(self)

    def changePolicy(self):
        self.allow = not self.allow

    def showAttributes(self):
        print(
            f"{self.name}: {self.allow}"
        )

    def returnAttributes(self):
        return f"{self.name};{self.allow}\n"

# A simple way to get a commune by its name.
def getCommune(name):
    result = next((c for c in listCommunes if c.name == name), None)
    if result:
        return result
    else:
        print('Error: no commune found with this name.')

# Show all objects.
def showObjects():
    showKG()
    showLaws()
    showCredits()
    showCommunes()
    showLines()

def showKG(g_height=500):
    return gv.d3(G, graph_height=g_height, show_edge_label=True, edge_label_data_source='label', node_drag_fix = True)

def showCommunes():
    for c in listCommunes:
        c.showAttributes()

def showCredits():
    for c in listCredits:
        c.showAttributes()

def showLaws():
    for l in listLaws:
        l.showAttributes()

def showLines():
    for l in listLines:
        l.showAttributes()

# Import and export
def importCommunes(path):
    communesDF = pd.read_csv(path, sep=';')
    communesDF.columns=['cname', 'allow']
    communesDF.apply(lambda x: Commune(x.cname, x.allow), axis=1)

def savecsv(path, contents, header=''):
    with open(path, 'w') as f:
        if header != '':
            f.write(header + '\n')
        f.write(contents)

def exportKG(blankstate=True):
    exportLaws(listLaws)
    exportCredits(listCredits)
    exportCommunes(listCommunes)
    exportLines(listLines, blankstate)

def exportLaws(listLaws):
    toSave = ""
    for law in listLaws:
        toSave += law.returnAttributes()
    savecsv('exports/laws.csv', toSave, header= 'name;dateVoted;dateEnforced;dateModified')

def exportCredits(listCredits):
    toSave = ""
    for credit in listCredits:
        toSave += credit.returnAttributes()
    savecsv('exports/credits.csv', toSave, header= 'name;date;parentLaw')

def exportCommunes(listCommunes):
    toSave = ""
    for commune in listCommunes:
        toSave += commune.returnAttributes()
    savecsv('exports/communes.csv', toSave, header= 'name;allow')

def exportLines(listLines, blankstate=True):
    if blankstate:
        shutil.rmtree('outshp')
    if not os.path.isdir('outshp'):
        os.mkdir('outshp')
    for line in listLines:
        line.exportSHP()
    linesGDF.to_file(f"outshp/all_lines_lineObj.shp")

def importModelOutputs(sim_folder, yearOffset=0, g_height=500):
    importCredits(sim_folder, yearOffset)
    importLines(sim_folder, yearOffset)
    return showKG(g_height)

def importCredits(simfolder, yearOffset=0):
    fp = f"model_outputs/{simfolder}/gen_credits.csv"
    with open(fp, 'r') as f:
        next(f)
        for line in f:
            line = line.split(';')
            date = line[2][:10]
            year = int(date[:4]) + yearOffset
            date = str(year) + date[4:]
            G.add_edge("Centralized government", line[1], label="approves")
            G.add_edge(line[1], line[0], label=f"creates on {date}")

def importLines(simfolder, yearOffset=0):
    fp = f"model_outputs/{simfolder}/gen_lines.csv"
    with open(fp, 'r') as f:
        next(f)
        for line in f:
            line = line.split(';')
            date = line[2][:10]
            year = int(date[:4]) + yearOffset
            date = str(year) + date[4:]
            G.add_edge(line[1], line[0], label=f"creates on {date}")


# Methods for reading data from an event table.

def append_shp(filename, parent_folder = "inshp"):
    if parent_folder:
        if filename[-4:] != '.shp':
            return f"{parent_folder}/{filename}.shp"
        else:
            return f"{parent_folder}/{filename}"
    else:
        if filename[-4:] != '.shp':
            return f"{filename}.shp"
        else:
            return filename

def isolate_line_id(line):
    if len(line) > 2:
        return line[-2:]
    else:
        return line

def reformat_dates(date, iso= True):
    if iso: 
        new_date = date.split(".")
        new_date.reverse()
        return '-'.join(new_date)
    else:
        return date.replace(".", "-")

def createObjectsFromTable(row):
    if len(listLaws) > 0:
        law = listLaws[0]
        credit = law.createCredit(name= row.Name, date= row.Date1)
        line = credit.createLine(number= row.Line, date= row.Date3, spatialData= row.shp)
        line.parseData()

def importEventTable(filename):

    df = pd.read_csv(filename, sep = ";")
    df.columns = ["Line", "Name", "Date1", "Date2", "Date3", "shp", "Comments"]

    df.shp = df.shp.apply(lambda x: append_shp(x))

    df.Line = df.Line.apply(lambda x: isolate_line_id(x))

    df.Date1 = df.Date1.apply(lambda x: reformat_dates(x))
    df.Date2 = df.Date2.apply(lambda x: reformat_dates(x))
    df.Date3 = df.Date3.apply(lambda x: reformat_dates(x))

    df.apply(lambda x: createObjectsFromTable(x), axis=1)

    if len(listLaws) == 0:
        print("Error: no law has been created yet. No credits or lines have been created.")
    else:
        print("Credits and lines created from input event table!")
